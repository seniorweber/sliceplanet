$(document).ready(function() {
    $('.slider__list').slick({
        //slidesToShow: 6,
        // slidesToScroll: 1,
        // variableWidth: true,
        dots: true,
        nextArrow: '<button type="button" class="item-next">Next</button>',
        prevArrow: '<button type="button" class="item-prev">Prev</button>',
        // //autoplay: true,
        // // autoplaySpeed: 2000
    });

    var $menu = $("#mobile-menu").mmenu({
        navbar: {
            title: "ПН-ПТ с 9:00 до 18:00"
        },
        "navbars": [
            {
                "position": "top",
                // "content": [
                //
                // ]
            }
        ]
    },{
        offCanvas: {
            pageSelector: ".main-wrapper"
        }
    });



    var $icon = $("#mobile-menu-icon");
    var API = $menu.data( "mmenu" );

    $icon.on( "click", function() {
        API.open();

    });

    API.bind( "open:finish", function() {
        setTimeout(function() {
            $icon.addClass( "is-active" );
        }, 100);
    });
    API.bind( "close:finish", function() {
        setTimeout(function() {
            $icon.removeClass( "is-active" );
        }, 100);
    });
});
